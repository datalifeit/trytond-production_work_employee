# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .employee import TeamWorkCenter, Team, EmployeeWorkCenter, Employee
from .production_work import WorkCenter
from .timesheet import TimesheetLine


def register():
    Pool.register(
        TimesheetLine,
        module='production_work_employee', type_='model',
        depends=['timesheet'])
    Pool.register(
        TeamWorkCenter,
        Team,
        EmployeeWorkCenter,
        Employee,
        WorkCenter,
        module='production_work_employee', type_='model')

