datalife_production_work_employee
=================================

The production_work_employee module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-production_work_employee/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-production_work_employee)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
