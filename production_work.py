# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction


class WorkCenter(metaclass=PoolMeta):
    __name__ = 'production.work.center'

    employee_allocs = fields.One2Many('company.employee-work.center',
       'work_center', 'Employee allocations',
       order=[('date', 'DESC')])
    many_employees = fields.Boolean('Many employees',
        help='It allows to configure many employees for the Work center.')
    employee = fields.Function(fields.Many2One('company.employee', 'Employee'),
        'get_employee')

    @staticmethod
    def default_many_employees():
        return False

    @classmethod
    def get_employee(cls, records, name=None):
        return cls.compute_employee(records, Transaction().context.get('date',
            None))

    @classmethod
    def compute_employee(cls, records, date):
        pool = Pool()
        EmployeeAlloc = pool.get('company.employee-work.center')

        if records and isinstance(records[0], int):
            records = cls.browse(records)
        res = dict.fromkeys(list(map(int, records)), None)
        if not date:
            return res

        _domain = ['OR', ]
        to_check = [r for r in records if not r.many_employees]
        for record in to_check:
            _domain.append([
                ('work_center', '=', record.id),
                ('date', '<=', date),
                ['OR',
                    ('end_date', '>=', date),
                    ('end_date', '=', None)]])

        if len(_domain) > 1:
            employees = EmployeeAlloc.search(_domain,
                order=[('work_center', 'ASC'), ('date', 'ASC')])
            res.update({e.work_center.id: e.employee.id for e in employees})

        return res
