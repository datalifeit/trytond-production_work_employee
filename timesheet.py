# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class TimesheetLine(metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    work_center = fields.Function(
        fields.Many2One('production.work.center', 'Work center'),
        'get_work_center')

    def get_work_center(self, name=None):
        if self.employee:
            wcenter = self.employee.compute_work_center(self.date)
            return wcenter.id if wcenter else None
        return None
