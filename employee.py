# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Bool
from trytond.pyson import Eval, If
from trytond.transaction import Transaction
from trytond.cache import Cache
from trytond.i18n import gettext
from trytond.exceptions import UserError
import datetime


class TeamWorkCenter(ModelSQL, ModelView):
    """Team - Work Center"""
    __name__ = 'company.employee.team-work.center'
    _table = 'company_employee_team_wcenter_rel'

    team = fields.Many2One('company.employee.team', 'Team', required=True,
        select=True, ondelete='RESTRICT')
    work_center = fields.Many2One('production.work.center', 'Work center',
        required=True)
    date = fields.Date('Date', required=True, select=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('team_wcenter_uniq',
             Unique(t, t.team, t.date),
             'production_work_employee.team_wcenter_uniq')]
        cls._order.insert(0, ('date', 'DESC'))

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    def delete(cls, team_work_centers):
        Team = Pool().get('company.employee.team')
        super(TeamWorkCenter, cls).delete(team_work_centers)
        Team._work_centers_cache.clear()

    @classmethod
    def create(cls, vlist):
        Team = Pool().get('company.employee.team')
        team_work_centers = super(TeamWorkCenter, cls).create(vlist)
        Team._work_centers_cache.clear()
        return team_work_centers

    @classmethod
    def write(cls, *args):
        Team = Pool().get('company.employee.team')
        super(TeamWorkCenter, cls).write(*args)
        Team._work_centers_cache.clear()


class Team(metaclass=PoolMeta):
    __name__ = 'company.employee.team'

    work_centers = fields.One2Many('company.employee.team-work.center', 'team',
        'Work centers')
    _work_centers_cache = Cache('company_employee_team.work_centers')
    work_center = fields.Function(
        fields.Many2One('production.work.center', 'Work center'),
        'get_work_center')

    def get_work_center(self, name=None):
        ctx_date = Transaction().context.get('date', None)
        work_center = self.compute_work_center(ctx_date)
        return work_center.id if work_center else None

    def get_work_centers(self):
        # Get from cache employee work centers or fetch them from the db
        work_centers = self._work_centers_cache.get(self.id)
        if work_centers is None:
            pool = Pool()
            TeamWorkCenter = pool.get('company.employee.team-work.center')
            team_work_centers = TeamWorkCenter.search([
                ('team', '=', self.id), ],
                order=[('date', 'ASC')])

            work_centers = []
            for team_work_center in team_work_centers:
                work_centers.append(
                    (team_work_center.date, team_work_center.work_center))
            self._work_centers_cache.set(self.id, work_centers)
        return work_centers

    def compute_work_center(self, date=None):
        """"Return the work center for the given date"""

        pool = Pool()
        Date = pool.get('ir.date')

        team_work_centers = self.get_work_centers()

        if date is None:
            date = Date.today()
        # search the work center for the given date
        work_center = None
        if team_work_centers and date >= team_work_centers[0][0]:
            work_center = None
            for tdate, twork_center in team_work_centers:
                if date >= tdate:
                    work_center = twork_center
                else:
                    break
        return work_center or None


class EmployeeWorkCenter(ModelSQL, ModelView):
    """Employee - Work Center"""
    __name__ = 'company.employee-work.center'
    _table = 'company_employee_wcenter_rel'

    employee = fields.Many2One('company.employee', 'Employee', required=True,
        select=True, ondelete='RESTRICT')
    work_center = fields.Many2One('production.work.center', 'Work center',
        required=True)
    date = fields.Date('Date', required=True, select=True,
        domain=[If(Bool(Eval('end_date')),
            ('date', '<=', Eval('end_date')), ())],
        depends=['end_date'])
    end_date = fields.Date('End date', select=True,
        domain=['OR', ('end_date', '=', None),
            ('end_date', '>=', Eval('date'))],
        depends=['date'])

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._order.insert(0, ('date', 'DESC'))

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
            ('work_center.rec_name', ) + tuple(clause[1:]),
            ('employee.party.name',) + tuple(clause[1:])]

    @classmethod
    def delete(cls, employee_work_centers):
        Employee = Pool().get('company.employee')
        super(EmployeeWorkCenter, cls).delete(employee_work_centers)
        Employee._work_centers_cache.clear()

    @classmethod
    def create(cls, vlist):
        Employee = Pool().get('company.employee')
        employee_work_centers = super(EmployeeWorkCenter, cls).create(vlist)
        Employee._work_centers_cache.clear()
        return employee_work_centers

    @classmethod
    def write(cls, *args):
        Employee = Pool().get('company.employee')
        super(EmployeeWorkCenter, cls).write(*args)
        Employee._work_centers_cache.clear()

    @classmethod
    def validate(cls, records):
        # check work center
        cls._check_restriction(records, 'work_center')

        # check employee
        cls._check_restriction(records, 'employee')
        super(EmployeeWorkCenter, cls).validate(records)

    @classmethod
    def _check_restriction(cls, records, field_name):
        values = {}
        # {field_id: {(date1,date2): record_id}}

        for record in records:
            if (field_name == 'work_center'
                    and record.work_center.many_employees):
                continue
            _field_value = getattr(record, field_name).id
            values.setdefault(_field_value, {})
            _key = (record.date, record.end_date)
            if not record.end_date:
                _key = 'default'
            if values[_field_value].get(_key):
                raise UserError(gettext(
                    'production_work_employee.msg_unique_%s' % field_name))
            if record.end_date:
                for _, _date_values in values.items():
                    for date_keys, ids in _date_values.items():
                        if not isinstance(date_keys, tuple):
                            continue
                        if ((record.date <= date_keys[1] <= record.end_date
                            and record.date > date_keys[0])
                        or (record.end_date >= date_keys[0] >= record.date
                                and record.end_date < date_keys[1])):
                            raise UserError(gettext(
                                'production_work_employee.'
                                'msg_unique_%s' % field_name))

            values[_field_value].setdefault(_key, record.id)

        # check persisted data
        for record_id, _date_values in values.items():
            _domain = [(field_name, '=', record_id)]
            ids = []
            subdomain = ['OR', ]
            for _key, record_id in _date_values.items():
                ids.append(record_id)
                if _key == 'default':
                    subdomain.append([('end_date', '=', None)])
                else:
                    subdomain.append([
                        ('date', '<=', _key[1]),
                        ('end_date', '>=', _key[1]),
                        ('date', '>', _key[0])])
                    subdomain.append([
                        ('date', '<=', _key[0]),
                        ('end_date', '>=', _key[0]),
                        ('end_date', '<', _key[1])])
            if ids:
                _domain.append(('id', 'not in', ids))
            if subdomain:
                _domain.append(subdomain)

            res = cls.search(_domain)
            if res:
                raise UserError(gettext(
                    'production_work_employee.msg_unique_%s' % field_name))


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    work_centers = fields.One2Many('company.employee-work.center', 'employee',
                                   'Work centers',
                                   order=[('date', 'DESC')])
    _work_centers_cache = Cache('company_employee.work_centers')
    work_center = fields.Function(
        fields.Many2One('production.work.center', 'Work center'),
        'get_work_center', searcher='search_work_center')

    def get_work_center(self, name=None):
        ctx_date = Transaction().context.get('date', None)
        work_center = self.compute_work_center(ctx_date)
        return work_center

    def get_work_centers(self):
        # Get from cache employee work centers or fetch them from the db
        work_centers = self._work_centers_cache.get(self.id)
        if work_centers is None:
            pool = Pool()
            EmployeeWorkCenter = pool.get('company.employee-work.center')
            employee_work_centers = EmployeeWorkCenter.search([
                ('employee', '=', self.id), ],
                order=[('date', 'ASC')])

            work_centers = []
            for employee_work_center in employee_work_centers:
                work_centers.append(
                    self._get_work_center_cache_key(employee_work_center))
            self._work_centers_cache.set(self.id, work_centers)
        return work_centers

    @classmethod
    def _get_work_center_cache_key(cls, employee_wc):
        return (
            employee_wc.date,
            employee_wc.end_date or datetime.date.max,
            employee_wc.work_center.id)

    def compute_work_center(self, date=None, **kwargs):
        """"Return the work center for the given date"""
        pool = Pool()
        Date = pool.get('ir.date')

        employee_work_centers = self.get_work_centers()

        if date is None:
            date = Date.today()
        # search the work center for the given date
        work_center = None
        if employee_work_centers and date >= employee_work_centers[0][0]:
            for edate, eend_date, ework_center in employee_work_centers:
                if date >= edate:
                    if date <= eend_date:
                        work_center = ework_center
                else:
                    break
        return work_center

    @classmethod
    def search_work_center(cls, name, clause):
        _date = Transaction().context.get('date', datetime.date.max)
        return [
            ('work_centers', 'where', [
                (name,) + tuple(clause[1:]),
                ('date', '<=', _date),
                ['OR', ('end_date', '>=', _date),
                 ('end_date', '=', None)]])]
